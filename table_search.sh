#!/bin/bash
######################################################################
#Copyright (C) 2024  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

# fatal uses SIGUSR1 to allow clean fatal errors
trap "exit 1" 10
PROC=$$

function error() {
	red=$(echo -en "\e[31m")
	normal=$(echo -en "\e[0m")

	echo -e "${red}$@${normal}" >&2
	#  exit 1
	kill -10 $PROC
}

[[ $1 ]] || error "Data Base needed.  Example: $0 mydata.db"
[[ -f "$1" ]] || error "'$1' does not exisit."
db="$1"

table="$(sqlite3 "$db" ".tables" | tr " " "\n" | sort -u | fzf --prompt="Select a Table: ")"
[[ $table ]] || exit

tmp="/tmp/sql-$RANDOM"

sqlite3 "$db" "select * from pragma_table_info('$table') as tblInfo;" | cut -d\| -f2 >"${tmp}_1"
sqlite3 "$db" "select * from $table" | fzf --prompt="Select Entry: " | tr "|" "\n" >"${tmp}_2"
paste "${tmp}_1" "${tmp}_2"
rm "${tmp}_1" "${tmp}_2"
